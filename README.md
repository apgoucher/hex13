This repository contains a program for simulating an artificial chemistry (AC)
on a CUDA-compatible GPU. The artificial chemistry was designed to be simple
and efficient, whilst embodying enough flexibility and rich interactions to
support open-ended evolution.

We recommend reading Tim Hutton's papers on artificial chemistries, which
served as the inspiration for this project:

 - [Evolvable Self-Replicating Molecules in an Artificial
Chemistry](http://www.sq3.org.uk/papers/evselfreps.pdf) (2002);
 - [Information-Replicating Molecules with Programmable
Enzymes](http://www.sq3.org.uk/papers/hc03.pdf) (2003);
 - [Replicators That Make All Their Own
Rules](http://www.sq3.org.uk/papers/aca05.pdf) (2005);

Our theoretical contribution is a simple rule exhibiting closure: no reactions
are hardcoded in the rule itself, so a self-sufficient organism must produce
enzymes expressing every reaction required for its own replication. By
completely eliminating hardcoded reactions from our rule, we essentially
impose no constraints on the structure of a self-replicator: it is likely
that the best replicator would look completely dissimilar to the DNA-based
and cell-like proposals considered here. Moreover, given a very long time,
we expect that the optimal replicator will evolve naturally from our
suboptimal replicators.

Our practical contribution is this program, for simulating our rule and other
lattice artificial chemistries on a GPU. The intention is to substantially
reduce the amount of time required for evolution by running large simulations
very quickly; our simulator runs about 100 times faster than an efficient
CPU-based implementation.

Prerequisites
-------------

You must have a CUDA-compatible GPU together with the compilers g++ and nvcc.
If your machine lacks a GPU (as mine does), an alternative is to spin up a
'p2.xlarge' or 'p3.2xlarge' instance on Amazon Web Services:

 - https://aws.amazon.com/ec2/instance-types/p2/
 - https://aws.amazon.com/ec2/instance-types/p3/

Execution speed depends on the worldsize and GPU. We recommend the default
size of $`832 \times 832`$ for the instances described above; the 'p3.2xlarge'
runs at 25000 ticks per second and the 'p2.xlarge' runs at 4000 ticks per
second for this worldsize.

If you are using a remote machine, make sure port 8888 is open to accept HTTP
traffic; otherwise, you won't be able to view the simulation through the
browser frontend.

Getting started
---------------

Clone the repository and run the following commands to get started:

    make clean
    make
    ./hex13.exe
    
(If you are on Windows, see CMakeLists.txt for build instructions.)

The output should resemble the following:

![](files/terminal.png)

where the 'epoch' counter increases continually at a steady rate. If you see
this behaviour, the simulation is running correctly.

Open a web browser at `http://localhost:8888/` (with 'localhost' replaced by
the public IPv4 address if you are using a remote machine) to watch the
simulation. There are a few command-line options which may be given to the
executable to specify the initial configuration:

 - -d, --density: proportion of sites to be populated with atoms;
 - -b, --background: file to indicate the background chemical composition;
 - -f, --foreground: initial pattern file to be loaded into the world.

In general, increasing the density will make reactions happen more quickly,
but a prohibitively large density could cause molecules to become trapped.
The default density is specified in the preferences file and is set to 0.25.

Preview
-------

Here is a snapshot of a small region of the artificial chemistry world:

![](files/browser.png)

We have a regular hexagonal lattice of _sites_, which we identify with the
ring of Eisenstein integers $`E := \{ a + b \omega : a, b \in \mathbb{Z} \}`$.
In particular, each site has six immediate _neighbours_ at a distance of 1;
the next-closest sites are each at a distance of $`\sqrt{3}`$.

In the diagram above, some of the sites contain _atoms_ (drawn as coloured
balls) and some of the atoms are joined to other atoms with _bonds_ (drawn as
thin black line segments joining the centres of two nearby atoms).

Description
-----------

Our artificial chemistry is inspired by Hutton (2002). We have a regular
lattice of hexagonal _sites_, each of which may be either empty or contain
a single _atom_. An atom has an immutable _type_ (usually represented by a
lowercase Roman letter, or graphically by its colour) and a variable _state_
(an unsigned 52-bit integer).

In addition to atoms, the other primitive objects are _bonds_. A bond is an
edge between two atoms in nearby sites; two atoms can be bonded only if the
distance between their centres is at most $`\sqrt{3}`$. We can view this from
the perspective of graph theory, considering atoms and bonds as vertices and
edges, respectively. We impose the further constraint that this graph is
planar: at no point are bonds allowed to cross.

It is convenient to think of bonds as being positioned halfway between the
two atoms they join. As such, each bond must lie on a point in the set of
Eisenstein half-integers, $`\frac{1}{2}E \setminus E`$.

We have two mechanisms by which the world evolves, namely _physics_ and
_chemistry_. The physics step allows an atom to move randomly to an empty
neighbouring site, provided doing so does not cause bonds to become crossed
or stretched beyond their maximum length of $`\sqrt{3}`$. The chemistry step
is where, mediated by a nearby _enzyme_ (atom of type 'h'), two adjacent atoms
can _react_ (alter their states and/or whether they are bonded) according to
a precise rule (or 'reaction') encoded in the state of the enzyme.

An example of a reaction is the following:

    a0 + b0 --> a1b1

It states that if an atom of type 'a' and state 0 meets an atom of type 'b'
and state 0, and they are not already bonded to each other, then the reaction
will cause them to become bonded and move into state 1. The meta-types 'x'
and 'y' can stand for arbitrary atom types; for instance:

    x0 + y0 --> x1y1

will cause any pair of disjoint state-0 atoms to bond (and enter state 1).
Alternatively, the reaction:

    x0 + x0 --> x1x1

will do the same, but with the restriction that both reactants must have the
same type. We also allow reactions to apply linear transformations to the
states of the atoms, such as:

    a0 + b(8 + n) --> a(6 + 2n) + b37

which essentially represents all reactions where 'n' ranges over the natural
numbers (including zero) such that all states mentioned by the reaction do
not exceed $`2^{52} - 1`$.

In general, a reaction may take the form:

    alpha(A) (+)? beta(B + En) --> alpha(C + Fn) (+)? beta(D)

where:

 - alpha and beta can each be one of the 62 atom types, or one of the two
   variable meta-types 'x' and 'y'.
 - A, B, C and D are each 8-bit unsigned integers.
 - the multipliers E and F can each be either 0, 1, 2, 4, 8, 16, 32, or 64.
 - there are two further independent binary choices, namely whether the atoms
   are bonded before, and whether the atoms are bonded afterwards.

Consequently, there are $`64^2 256^4 8^2 2^2 = 2^{52}`$ different possible
reactions. An enzyme (atom of type 'h') can have one of $`2^{52}`$ different
states, which determines exactly which one of the possible reactions it
catalyses.

Without the multipliers E and F, it would only be possible to refer to states
between 0 and 255. Linear transformation reactions with non-zero E and F are
necessary to result in the production of atoms with states exceeding 255.
Since enzymes will, in general, make use of the full 52-bit space, this is
necessary to attain closure: an organism can synthesise all of the enzymes it
needs to both replicate and synthesise its own enzymes.

Browser frontend
----------------

When viewing the simulation in the browser, atoms are represented by circular
discs whose colour indicates the atom type. Bonds are shown as black lines
connecting atoms. The frontend repeatedly queries the simulator and updates
the viewport accordingly. The viewport is only a small window into the world
being simulated on the GPU (the default worldsize being a $`832 \times 832`$
torus); you can scroll around the world by use of the arrowkeys.

The frontend also tracks the number of epochs elapsed since the beginning of
the simulation. For convenience, the frontend files hex13.html and hex13.js
are automatically served by the same program which simulates the artificial
chemistry; consequently, http://localhost:8888 is only accessible when the
simulation is running.

If you open port 8888 to the Internet, then other people can watch your
simulation by replacing 'localhost' with your IP address.

Simulator
---------

The simulator is written in C++, with GPU code written in CUDA. At each
timestep (or _tick_), the simulator partitions the grid into a tessellation
of many 13-hexagon _clusters_ and instructs the GPU to simulate the physics
and chemistry update rules on every cluster in parallel (with one GPU thread
per cluster). There are 13 different choices of partition, which differ from
each other by a translation:

![](files/clusters.gif)

An _epoch_ is a set of 156 successive ticks, which chooses each of the 13
different partitions exactly 12 times. The epoch uses a de-Bruijn-like
sequence which contains every ordered pair of two distinct partitions exactly
once, so there are exactly $`13 \times 12 = 156`$ ticks per epoch.

The atom sites form a hexagonal lattice called the _atom lattice_, which we
identify with the ring E of Eisenstein integers in the obvious way. We also
consider the set $`\frac{1}{2}E \setminus E`$ of Eisenstein half-integers,
which we call the _bond lattice_. Each point on the bond lattice can either
contain no bond, or a 'short bond' of length 1, or a 'long bond' of length
$`\sqrt{3}`$. We represent this using 2 bits of information: the presence or
absence of a short bond, and the presence or absence of a long bond. (Only the
bit-patterns 00, 01, and 10 are possible, since a short and long bond cannot
coexist in the same lattice point without violating the crossing rule.)

For each atom site x in E, we combine the 52-bit atom state, the 6-bit atom
type (which can be any of the 62 atom types, or empty space, or a wall), and
the 2-bit bond information from each of the bond sites $`x + \frac{1}{2}`$,
$`x + \frac{1}{2} \omega`$, and $`x + \frac{1}{2} \omega^2`$ into a 64-bit
unsigned integer. By specifying this 64-bit integer for each atom site in the
lattice, we have a complete description of the entire state of the world.
Coupled with the fact that the rules are local, the simulation is a $`2^{64}`$
state deterministic cellular automaton with a non-deterministic partitioning
scheme (the aforementioned random shuffling of ticks within an epoch).

Given a cluster of 13 atom sites represented by their 64-bit values, the CUDA
kernel (running on a GPU thread) performs the following operations:

 - Unpacks the atom and bond information from the bit-packed encoding;
 - Creates a 64-bit hash H of the thirteen 64-bit values;
 - Determines whether there is a non-zero enzyme in the cluster and, if so,
   attempts to find a suitable reaction;
 - Ascertains whether the central site contains an atom and, if so, attempts
   to move it to a random one of six neighbours (determined pseudorandomly
   from the hash H);
 - Checks whether the central site is empty and, if so, stores 52 bits of H
   back into the 52 unused 'state' bits of the empty site (thereby enabling
   the empty space to function as a huge pseudorandom number generator);
 - Repacks the atom and bond information back into the lattice.

The complete application of this kernel to every cluster in the partition
constitutes a _tick_, thirteen of which together form an _epoch_.

Configuration
-------------

You can configure simulation preferences by performing the following steps:

 - Alter the `#define` statements in **scripts/hex13prefs.py**;
 - Run `make clean` to clear out temporary files;
 - Run `make` to recompile the new hex13 executable.

The variables in **scripts/hex13prefs.py** include the world size, labels
for the 62 atom types, and whether bonds are allowed to cross. You can
import the preferences into a Python script (in the scripts directory) by
calling:

    from hex13prefs import prefdict

You can then access the world size as `prefdict["GRIDSIZE"]` and the labels
of the atom types as `prefdict["ATOMTYPES"]`. You can similarly determine
whether bonds are allowed to cross by evaluating `("CROSSBONDS" in prefdict)`.

It can also be imported into C++ code (and indeed _is_ imported by the hex13
simulator) by means of the following preprocessor directive:

    #include "scripts/hex13prefs.py"

Alternatively, to simply _display_ the dictionary of preferences, execute:

    scripts/hex13prefs.py

in either Bash or Python.

Patterns and backgrounds
------------------------

Recall that at runtime you can specify the foreground and background files
to load into the simulator. These are typically stored in the `patterns`
and `backgrounds` directories, respectively, and commonly use the filename
extensions `.h13` and `.bkg`.

In both types of file, the file is read as a whitespace-delimited list of
commands. A command can be an atom description (type followed by state) in
either foreground or background files. Background files may also contain
relative abundance specifiers (such as `3.5x`), which are self-explanatory.

Foreground files are slightly more complicated. You can jump to a point
$`x + y \omega`$ by the command `@x,y`, where you can create a new molecule
by specifying a list of atoms and using other commands to change direction
(e.g. `>0` for right, `>3` for down, `>6` for left, and `>9` for up). Also,
one may backtrack by specifying `!n`, where n is the number of atoms to
backtrack; this allows the construction of dendritic molecules.

Atom descriptions may be in either decimal or hexadecimal, so `a42` and
`a0x2a` are both valid ways to specify the same atom.

Rules
-----

The precise way in which reactions are determined from the 52-bit state of
an enzyme atom is specified in a _rule file_. The linear chemistry we
described above:

    alpha(A) (+)? beta(B + En) --> alpha(C + Fn) (+)? beta(D)

is actually not hardcoded into the source code, but is instead implemented
in the file `rules/linear.rule`. Rule files specify how an enzyme state is
interpreted, and how the states and bondedness change as a function of the
atom's current states. It would be possible to create a rule file to enable
chemistries of the form:

    alpha(A) (+)? beta(B + En) --> alpha(C + Fn) (+)? beta(D + Gn)

by reserving 2 bits for each of E, F, and G, instead of 3 bits for each of
E and F. If one were to implement it and save it as `rules/bilinear.rule`
(for example), it could then be imported by changing the line in the hex13
preferences file to:

    #define RULENAME "bilinear.rule"

Rule files are written in CUDA C, as they are included directly into the
kernel at compilation time.
