#pragma once

#include "hex13.h"
#include <shared_mutex>
#include <vector>
#include <algorithm>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include <iterator>
#include <chrono>
#include <random>
#include <map>

#define NORMALISE_64 5.421010862427522e-20

std::string bondstring(int x1, int y1, int x2, int y2) {

    std::ostringstream oss2;
    oss2 << "[[" << x1 << ", " << y1 << "], [" << x2 << ", " << y2 << "]]";
    return oss2.str();

}

void output_to_json(std::vector<std::vector<uint64_t> > &atomdata, std::ostringstream &oss) {

    std::vector<std::string> atoms;
    std::vector<std::string> bonds;

    for (int64_t y = 0; y < ((int64_t) atomdata.size()); y++) {
        for (int64_t x = 0; x < ((int64_t) atomdata[y].size()); x++) {
            uint64_t z = atomdata[y][x];
            if (z & 4032) {
                uint64_t state = z >> 12;
                uint64_t type = (z & 4032) >> 6;
                char ts = ATOMTYPES[type];
                std::ostringstream oss2;
                oss2 << "{\"coords\": [" << x << ", " << y << "], ";
                oss2 << "\"type\": \"" << ts << "\", \"state\": \"" << state << "\"}";
                atoms.push_back(oss2.str());
            }
            if (z & 1) { bonds.push_back(bondstring(x, y, x+1, y)); }
            if (z & 2) { bonds.push_back(bondstring(x, y-1, x+1, y+1)); }
            if (z & 4) { bonds.push_back(bondstring(x, y, x, y+1)); }
            if (z & 8) { bonds.push_back(bondstring(x-1, y, x+1, y+1)); }
            if (z & 16) { bonds.push_back(bondstring(x, y, x-1, y-1)); }
            if (z & 32) { bonds.push_back(bondstring(x-1, y, x, y-1)); }
        }
    }

    if (atoms.size() > 0) {
        oss << "\"atoms\": [\n";
        for (uint64_t i = 0; i < atoms.size(); i++) {
            oss << atoms[i] << ((i + 1 == atoms.size()) ? "],\n" : ",\n");
        }
    } else {
        oss << "\"atoms\": [],\n";
    }
    if (bonds.size() > 0) {
        oss << "\"bonds\": [\n";
        for (uint64_t i = 0; i < bonds.size(); i++) {
            oss << bonds[i] << ((i + 1 == bonds.size()) ? "],\n" : ",\n");
        }
    } else {
        oss << "\"bonds\": [],\n";
    }

}

template<typename Out>
void ssplit(const std::string &s, char delim, Out result) {
    // String splitting helper function:
    std::stringstream ss;
    ss.str(s);
    std::string item;
    while (std::getline(ss, item, delim)) {
        *(result++) = item;
    }
}

std::vector<std::string> ssplit(const std::string &s, char delim) {
    // Split a string on a delimiter:
    std::vector<std::string> elems;
    ssplit(s, delim, std::back_inserter(elems));
    return elems;
}

std::vector<std::string> spliturl(std::string url) {
    // Split a URL into path components:
    std::string url2 = url;
    while ((url2.size() > 0) && (url2[0] == '/')) { url2 = url2.substr(1); }
    return ssplit(url2, '/');
}

class mutex13world {
    /*
    * A hex13world guarded by a C++17 std::shared_mutex.
    *
    * This enables multiple threads to access subrectangles of the grid
    * whilst the GPU is busy computing in the background. When the GPU
    * has finished, the mutex is momentarily locked whilst the epoch
    * count and host memory are updated.
    */

    public:
    hex13world hw;
    uint64_t epoch;
    mutable std::shared_timed_mutex m;
    std::map<std::string, std::string> filemap;

    std::string slurp(std::ifstream& in) {
        std::stringstream sstr;
        sstr << in.rdbuf();
        return sstr.str();
    }

    void loadfile(std::string filename) {
        std::ifstream f;
        f.open("files/" + filename);
        filemap.emplace(filename, slurp(f));
    }

    mutex13world() {
        epoch = 0;
    }

    double evolve(uint64_t iters) {
        /*
        * Evolve the simulation for a specified number of epochs and safely
        * update the host memory after the GPU has completed. This should be
        * invoked repeatedly from the main loop of the master thread.
        */

        auto t1 = std::chrono::high_resolution_clock::now();

        for (uint64_t i = 0; i < iters; i++) {
            hw.performEpoch();
        }
        hw.waitForDevice();

        m.lock();
        epoch += iters;
        hw.copyDeviceToHost();
        hw.waitForDevice();
        auto t2 = std::chrono::high_resolution_clock::now();
        auto int_us = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1);
        double dpe = (1.0e6 * iters) / int_us.count();
        std::fprintf(stderr, "Epoch %10lu completed (%9.2f ticks per second)\r", epoch, dpe * 156);
        m.unlock();

        return dpe;
    }

    void copyHostToDevice() {
        m.lock_shared();
        hw.copyHostToDevice();
        m.unlock_shared();
    }

    std::pair<int64_t, int64_t> movedir(std::pair<int64_t, int64_t> p, int dir) {
        int64_t x = p.first; int64_t y = p.second;
        switch (dir) {
            case 0: x += 1; y += 0; break;
            case 1: x += 2; y += 1; break;
            case 2: x += 1; y += 1; break;
            case 3: x += 1; y += 2; break;
            case 4: x += 0; y += 1; break;
            case 5: x += -1; y += 1; break;
            case 6: x += -1; y += 0; break;
            case 7: x += -2; y += -1; break;
            case 8: x += -1; y += -1; break;
            case 9: x += -1; y += -2; break;
            case 10: x += 0; y += -1; break;
            case 11: x += 1; y += -1; break;
        }
        x = ((x % GRIDSIZE) + GRIDSIZE) % GRIDSIZE;
        y = ((y % GRIDSIZE) + GRIDSIZE) % GRIDSIZE;
        return std::pair<int64_t, int64_t>(x, y);
    }

    void emplaceBond(int64_t x0, int64_t y0, int dir) {
        int64_t x = x0; int64_t y = y0; uint64_t bitmask = 0;
        switch (dir) {
            case 0: bitmask = 1; break;
            case 1: bitmask = 8; x += 1; break;
            case 2: bitmask = 16; x += 1; y += 1; break;
            case 3: bitmask = 2; y += 1; break;
            case 4: bitmask = 4; break;
            case 5: bitmask = 32; y += 1; break;
            case 6: bitmask = 1; x -= 1; break;
            case 7: bitmask = 8; x -= 1; y -= 1; break;
            case 8: bitmask = 16; break;
            case 9: bitmask = 2; x -= 1; y -= 1; break;
            case 10: bitmask = 4; y -= 1; break;
            case 11: bitmask = 32; x += 1; break;
        }
        x = ((x % GRIDSIZE) + GRIDSIZE) % GRIDSIZE;
        y = ((y % GRIDSIZE) + GRIDSIZE) % GRIDSIZE;
        hw.h_A[y][x] |= bitmask;
    }

    void loadPattern(std::string filename, std::pair<int64_t, int64_t> iicoord) {

        std::vector<std::pair<int64_t, int64_t> > cvec;
        int dir = 0;
        std::pair<int64_t, int64_t> icoord = iicoord;

        std::string atypes = ATOMTYPES;
        std::map<char, uint64_t> chartoint;
        for (uint64_t i = 0; i < atypes.size(); i++) {
            chartoint.emplace(atypes[i], i);
        }

        std::ifstream f;
        f.open(filename);
        std::string w;

        m.lock();
        while (f >> w) {
            if (w.size() > 0) {
                char c = w[0];
                if (chartoint.count(c)) {
                    uint64_t atype = chartoint[c];
                    uint64_t astate = std::stoll(w.substr(1), 0, 0);
                    std::pair<int64_t, int64_t> ccoord = icoord;
                    if (cvec.size() > 0) {
                        ccoord = cvec.back();
                        emplaceBond(ccoord.first, ccoord.second, dir);
                        ccoord = movedir(ccoord, dir);
                    }
                    cvec.push_back(ccoord);
                    hw.h_A[ccoord.second][ccoord.first] &= 63;
                    hw.h_A[ccoord.second][ccoord.first] |= (atype << 6);
                    hw.h_A[ccoord.second][ccoord.first] |= (astate << 12);
                } else if (c == '!') {
                    uint64_t k = 1;
                    if (w.size() > 1) { k = std::stoll(w.substr(1)); }
                    for (uint64_t i = 0; i < k; i++) {
                        if (cvec.size() == 0) { break; }
                        cvec.pop_back();
                    }
                } else if (c == '>') {
                    dir = std::stoll(w.substr(1));
                } else if (c == '@') {
                    std::vector<std::string> rcoords = ssplit(w.substr(1), ',');
                    icoord.first = iicoord.first + std::stoll(rcoords[0]);
                    icoord.second = iicoord.second + std::stoll(rcoords[1]);
                    cvec.clear();
                }
            }
        }
        hw.copyHostToDevice();
        m.unlock();

    }

    void compRandom(std::vector<std::pair<uint64_t, double> > composition, double density) {
        /*
        * Randomly seed the universe according to a list of atoms and
        * corresponding frequencies:
        */

        std::map<double, uint64_t> cum;

        double rt = 0;
        for (uint64_t i = 0; i < composition.size(); i++) {
            rt += composition[i].second;
            cum[rt] = composition[i].first;
        }

        double mfac = (rt * NORMALISE_64) / density;
        std::random_device rd;
        std::mt19937_64 gen(rd());
        std::uniform_int_distribution<uint64_t> dis;

        m.lock();
        for (uint32_t y = 0; y < GRIDSIZE; y++) {
            for (uint32_t x = 0; x < GRIDSIZE; x++) {
                uint64_t k = dis(gen);
                double z = k * mfac;
                auto it = cum.upper_bound(z);
                if (it == cum.end()) {
                    k = k & 0xfffffffffffff000ull;
                } else {
                    k = it->second;
                }
                hw.h_A[y][x] = k;
            }
        }
        hw.copyHostToDevice();
        m.unlock();
    }

    void loadBackground(std::string filename, double density) {

        std::string atypes = ATOMTYPES;
        std::map<char, uint64_t> chartoint;
        for (uint64_t i = 0; i < atypes.size(); i++) {
            chartoint.emplace(atypes[i], i);
        }

        std::ifstream f;
        f.open(filename);
        std::string w;

        std::vector<std::pair<uint64_t, double> > composition;

        double proportion = 1.0;
        while (f >> w) {
            if (w.size() > 0) {
                char c = w[w.size() - 1];
                if (c == 'x') {
                    proportion = std::stod(w.substr(0, w.size() - 1));
                } else {
                    uint64_t d = chartoint[w[0]] << 6;
                    d |= (((uint64_t) std::stoll(w.substr(1), 0, 0)) << 12);
                    composition.emplace_back(d, proportion);
                }
            }
        }

        compRandom(composition, density);
    }

    void floodSubrect(uint32_t left, uint32_t top, uint32_t width, uint32_t height, uint64_t bitmask) {
        /*
        * Breaks all bonds and zeroes all states within a rectangle.
        */
        m.lock();
        for (uint32_t y = top; y < top + height; y++) {
            for (uint32_t x = left; x < left + width; x++) {
                hw.h_A[y][x] &= bitmask;
            }
        }
        hw.copyHostToDevice();
        m.unlock();
    }

    void periodicFlood(uint64_t iteration, uint64_t log4frac) {
        uint32_t rs = GRIDSIZE >> log4frac;
        uint32_t x = iteration & ((1 << log4frac) - 1);
        uint32_t y = (iteration >> log4frac) & ((1 << log4frac) - 1);
        floodSubrect(rs * x, rs * y, rs, rs, 4032);
    }

    std::pair<uint64_t, std::vector<std::vector<uint64_t> > > getSubrect(uint64_t oldepoch,
        int left, int top, uint32_t width, uint32_t height) {
        /*
        * Get a rectangular region of the torus with wraparound. If the
        * host memory has not been updated since the last call (i.e. if
        * epoch == oldepoch), then the function will return an empty
        * rectangle. This means that clients requesting frames more
        * quickly than the host can handle will not consume too many
        * resources (and, in particular, will not hog the shared_mutex).
        */

        uint32_t x = ((left % GRIDSIZE) + GRIDSIZE) % GRIDSIZE;
        uint32_t y = ((top % GRIDSIZE) + GRIDSIZE) % GRIDSIZE;

        std::vector<std::vector<uint64_t> > rect;

        m.lock_shared();

        uint64_t newepoch = epoch;
        if (oldepoch < newepoch) {
            for (uint32_t i = 0; i < height; i++) {
                uint32_t row = (y + i) % GRIDSIZE;
                if (x + width <= GRIDSIZE) {
                    rect.emplace_back(hw.h_A[row] + x, hw.h_A[row] + x + width);
                } else {
                    rect.emplace_back(hw.h_A[row] + x, hw.h_A[row] + GRIDSIZE);
                    uint32_t w = x + width - GRIDSIZE;
                    while (w > GRIDSIZE) {
                        rect.back().insert(rect.back().end(), hw.h_A[row], hw.h_A[row] + GRIDSIZE);
                        w -= GRIDSIZE;
                    }
                    rect.back().insert(rect.back().end(), hw.h_A[row], hw.h_A[row] + w);
                }
            }
        }
        m.unlock_shared();

        return std::pair<uint64_t, std::vector<std::vector<uint64_t> > >(newepoch, rect);
    }

    std::string respondSubrect(std::vector<std::string> pathparts) {

        std::ostringstream oss;

        if (pathparts.size() < 6) {
            return "{\"exception\": \"invalid parameters\"}\n";
        }

        try {
            auto result = getSubrect(std::stoll(pathparts[1]), std::stoll(pathparts[2]), std::stoll(pathparts[3]), std::stoll(pathparts[4]), std::stoll(pathparts[5]));

            oss << "{\n";
            if (result.second.size() > 0) { output_to_json(result.second, oss); }
            oss << "\"epoch\": " << result.first << ",\n";
            oss << "\"worldsize\": " << GRIDSIZE << "\n";
            oss << "}\n";
            return oss.str();
        } catch(std::exception const & e) {
            return "{\"exception\": \"invalid parameters\"}\n";
        }
    }

};
