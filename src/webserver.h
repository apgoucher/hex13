#include "mutex13.h"

#include <sys/types.h>
#ifndef _WIN32
#include <sys/select.h>
#include <sys/socket.h>
#else
#include <winsock2.h>
#endif

#include <microhttpd.h>

#define PORT 8888

static auto answer_to_connection (void *cls, struct MHD_Connection *connection,
                      const char *url, const char *method,
                      const char *version, const char *upload_data,
                      size_t *upload_data_size, void **con_cls) {

    // Discard unused parameters to suppress compiler warnings:
    (void) method;
    (void) version;
    (void) upload_data;
    (void) upload_data_size;
    (void) con_cls;

    mutex13world* mw = (mutex13world*) cls;

    // Treat an empty path as /hex13.html, and an unknown path by giving
    // an unknown endpoint exception in the JSON.
    std::vector<std::string> pathparts = spliturl(url);
    std::string page = "{\"exception\": \"unknown endpoint\"}\n";
    if (pathparts.size() == 0) { pathparts.push_back("hex13.html"); }

    if (pathparts[0] == "sinoatrial") {
        /*
        * Query this endpoint on a periodic basis to check that the webserver
        * is still operative. This is intended for future expansion whereby
        * people can check a page on Catagolue for a list of alive hex13
        * servers.
        */
        page = "{\"heartbeat\": true}\n";
    } else if (pathparts[0] == "subrect") {
        /*
        * This takes a 5-tuple of values: (epoch, left, top, width, height)
        * If the current epoch exceeds the epoch in the path, the rectangle
        * specified by the other four values is returned.
        */
        page = mw->respondSubrect(pathparts);
    } else if (mw->filemap.count(pathparts[0])) {
        /*
        * This serves a static file provided it has been loaded. The primary
        * reason is to serve the hex13.html and hex13.js files.
        */
        page = mw->filemap[pathparts[0]];
    }

    const char* page2 = page.c_str();
    struct MHD_Response *response;
  
    response = MHD_create_response_from_buffer (page.size(), (void *) page2, MHD_RESPMEM_MUST_COPY);
    auto ret = MHD_queue_response (connection, MHD_HTTP_OK, response);
    MHD_destroy_response (response);

    return ret;
}

class mhdserver {

    private:
    struct MHD_Daemon *daemon;

    public:
    mhdserver() {
        daemon = 0;
    }

    void start(mutex13world *mw) {
        if (daemon == 0) {
            daemon = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION, PORT, NULL, NULL,
                &answer_to_connection, ((void*) mw), MHD_OPTION_END);
        }
    }

    void stop() {
        if (daemon != 0) {
            MHD_stop_daemon(daemon);
            daemon = 0;
        }
    }

    ~mhdserver() {
        stop();
    }

};
