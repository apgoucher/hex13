/*1:*/
#line 10 "./monolith.w"

__global__ void doTick(uint64_cu A[GRIDSIZE][GRIDSIZE],uint32_cu offset){

/*2:*/
#line 48 "./monolith.w"


uint32_cu prei= blockIdx.x*TBSIZE+threadIdx.x;
uint32_cu prej= blockIdx.y*TBSIZE+threadIdx.y;
uint32_cu x0= (prei*4+prej*3+offset)%GRIDSIZE;
uint32_cu y0= (prei+prej*4)%GRIDSIZE;









uint32_cu x1= (x0==(GRIDSIZE-1))?0:x0+1;
uint32_cu y1= (y0==(GRIDSIZE-1))?0:y0+1;
uint32_cu x2= (x1==(GRIDSIZE-1))?0:x1+1;
uint32_cu y2= (y1==(GRIDSIZE-1))?0:y1+1;
uint32_cu x3= (x2==(GRIDSIZE-1))?0:x2+1;
uint32_cu y3= (y2==(GRIDSIZE-1))?0:y2+1;
uint32_cu x4= (x3==(GRIDSIZE-1))?0:x3+1;
uint32_cu y4= (y3==(GRIDSIZE-1))?0:y3+1;

/*:2*/
#line 13 "./monolith.w"


/*3:*/
#line 84 "./monolith.w"


__shared__ uint64_cu B[13][TBSIZE][TBSIZE];
#define SPIRATOM(X)B[X][threadIdx.y][threadIdx.x]

SPIRATOM(0)= A[y2][x3];
SPIRATOM(1)= A[y3][x4];
SPIRATOM(2)= A[y3][x3];
SPIRATOM(3)= A[y4][x3];
SPIRATOM(4)= A[y3][x2];
SPIRATOM(5)= A[y3][x1];
SPIRATOM(6)= A[y2][x1];
SPIRATOM(7)= A[y1][x0];
SPIRATOM(8)= A[y1][x1];
SPIRATOM(9)= A[y0][x1];
SPIRATOM(10)= A[y1][x2];
SPIRATOM(11)= A[y1][x3];
SPIRATOM(12)= A[y2][x2];

/*:3*/
#line 15 "./monolith.w"

/*5:*/
#line 136 "./monolith.w"


uint32_cu innerbonds= 0;
innerbonds|= SPIRATOM(0)&48;
innerbonds|= SPIRATOM(10)&12;
innerbonds|= SPIRATOM(8)&3;
innerbonds= innerbonds<<6;
innerbonds|= SPIRATOM(12)&48;
innerbonds|= SPIRATOM(8)&12;
innerbonds|= SPIRATOM(6)&3;
innerbonds= innerbonds<<6;
innerbonds|= SPIRATOM(4)&48;
innerbonds|= SPIRATOM(12)&12;
innerbonds|= SPIRATOM(4)&3;
innerbonds= innerbonds<<6;
innerbonds|= SPIRATOM(2)&48;
innerbonds|= SPIRATOM(0)&12;
innerbonds|= SPIRATOM(12)&3;
uint32_cu newinner= innerbonds;
uint32_cu outerbonds= 0;
outerbonds|= SPIRATOM(1)&48;
outerbonds|= SPIRATOM(11)&12;
outerbonds|= SPIRATOM(10)&3;
outerbonds= outerbonds<<6;
outerbonds|= SPIRATOM(10)&48;
outerbonds|= SPIRATOM(9)&12;
outerbonds|= SPIRATOM(7)&3;
outerbonds= outerbonds<<6;
outerbonds|= SPIRATOM(6)&60;
outerbonds|= SPIRATOM(5)&3;
outerbonds= outerbonds<<6;
outerbonds|= SPIRATOM(3)&48;
outerbonds|= SPIRATOM(2)&15;
uint32_cu newouter= outerbonds;

/*:5*/
#line 16 "./monolith.w"

/*7:*/
#line 217 "./monolith.w"


uint64_cu H= SPIRATOM(12)+57885161;

{

uint64_cu h1= SPIRATOM(1)^SPIRATOM(5)^SPIRATOM(9);
uint64_cu h2= SPIRATOM(3)^SPIRATOM(7)^SPIRATOM(11);

uint64_cu k1= SPIRATOM(0);
uint64_cu k2= SPIRATOM(2);
k1*= 0x87c37b91114253d5ull;k1= ROTL64(k1,31);k1*= 0x4cf5ad432745937full;h1^= k1;
h1= ROTL64(h1,27);h1+= h2;h1= h1*5+0x52dce729ull;
k2*= 0x4cf5ad432745937full;k2= ROTL64(k2,33);k2*= 0x87c37b91114253d5ull;h2^= k2;
h2= ROTL64(h2,31);h2+= h1;h2= h2*5+0x38495ab5ull;

k1= SPIRATOM(4);
k2= SPIRATOM(6);
k1*= 0x87c37b91114253d5ull;k1= ROTL64(k1,31);k1*= 0x4cf5ad432745937full;h1^= k1;
h1= ROTL64(h1,27);h1+= h2;h1= h1*5+0x52dce729ull;
k2*= 0x4cf5ad432745937full;k2= ROTL64(k2,33);k2*= 0x87c37b91114253d5ull;h2^= k2;
h2= ROTL64(h2,31);h2+= h1;h2= h2*5+0x38495ab5ull;

k1= SPIRATOM(8);
k2= SPIRATOM(10);
k1*= 0x87c37b91114253d5ull;k1= ROTL64(k1,31);k1*= 0x4cf5ad432745937full;h1^= k1;
h1= ROTL64(h1,27);h1+= h2;h1= h1*5+0x52dce729ull;
k2*= 0x4cf5ad432745937full;k2= ROTL64(k2,33);k2*= 0x87c37b91114253d5ull;h2^= k2;
h2= ROTL64(h2,31);h2+= h1;h2= h2*5+0x38495ab5ull;

h1^= h1>>33;
h1*= 0xff51afd7ed558ccdull;
h1^= h1>>33;
h1*= 0xc4ceb9fe1a85ec53ull;
h1^= h1>>33;

h2^= h2>>33;
h2*= 0xff51afd7ed558ccdull;
h2^= h2>>33;
h2*= 0xc4ceb9fe1a85ec53ull;
h2^= h2>>33;

H+= (h1^h2);
}

/*:7*/
#line 17 "./monolith.w"


/*9:*/
#line 297 "./monolith.w"


#define DEFINE_ENZYMES
#include "loadrule.inc"
#undef DEFINE_ENZYMES







#ifndef REACTION_BITS
#define REACTION_BITS 12
#endif

#define UPDATE_REACTION(X) r2 =  SPIRATOM(X); r2 =  (IS_ENZYME((r2 >> 6))) ? r2 : 0; reaction =  (r2 >= (1 << REACTION_BITS)) ? r2 : reaction

uint64_cu reaction= 0;
{







uint64_cu r2;

UPDATE_REACTION(12);
UPDATE_REACTION(0);
UPDATE_REACTION(6);
UPDATE_REACTION(10);
UPDATE_REACTION(4);
UPDATE_REACTION(8);
UPDATE_REACTION(2);
UPDATE_REACTION(1);
UPDATE_REACTION(7);
UPDATE_REACTION(11);
UPDATE_REACTION(5);
UPDATE_REACTION(9);
UPDATE_REACTION(3);


reaction= reaction>>REACTION_BITS;
}

/*:9*/
#line 19 "./monolith.w"

/*10:*/
#line 359 "./monolith.w"


if(reaction!=0){

#define DECODE_REACTION
#include"loadrule.inc"
#undef DECODE_REACTION

/*14:*/
#line 396 "./monolith.w"


#define CHECK_FIRST_ATOM
#include"loadrule.inc"
#undef CHECK_FIRST_ATOM




uint32_cu rdir= (H>>7)&0xffffff;
rdir= rdir%6;


rdir= (VERIFY_ATOM(SPIRATOM(rdir*2)))?rdir:rdir+1;
rdir= (rdir> 5)?0:rdir;
rdir= (VERIFY_ATOM(SPIRATOM(rdir*2)))?rdir:rdir+1;
rdir= (rdir> 5)?0:rdir;
rdir= (VERIFY_ATOM(SPIRATOM(rdir*2)))?rdir:rdir+1;
rdir= (rdir> 5)?0:rdir;
rdir= (VERIFY_ATOM(SPIRATOM(rdir*2)))?rdir:rdir+1;
rdir= (rdir> 5)?0:rdir;
rdir= (VERIFY_ATOM(SPIRATOM(rdir*2)))?rdir:rdir+1;
rdir= (rdir> 5)?0:rdir;
rdir= (VERIFY_ATOM(SPIRATOM(rdir*2)))?rdir:6;

/*:14*/
#line 367 "./monolith.w"

if(rdir<6){

/*12:*/
#line 386 "./monolith.w"


newinner= ROTR24(newinner,4*rdir)&0xffffff;
newouter= ROTR24(newouter,4*rdir)&0xffffff;

/*:12*/
#line 370 "./monolith.w"

/*11:*/
#line 382 "./monolith.w"


uint32_cu type1= (SPIRATOM(rdir*2)>>6)&63;

/*:11*/
#line 371 "./monolith.w"

/*15:*/
#line 421 "./monolith.w"


#ifdef CROSSBONDS
#define CBMASK 1
#else
#define CBMASK 3
#endif




int dir2= -1;
int rdir2= rdir*2+1;
{
uint32_cu typevec= (SPIRATOM(12)&4032)>>6;
typevec|= (SPIRATOM(rdir2)&4032);
rdir2= (rdir2>=11)?(rdir2-11):(rdir2+1);
typevec|= ((SPIRATOM(rdir2)&4032)<<6);
rdir2= (rdir2>=4)?(rdir2-4):(rdir2+8);
typevec|= ((SPIRATOM(rdir2)&4032)<<12);
rdir2= (rdir2>=11)?(rdir2-11):(rdir2+1);
typevec|= ((SPIRATOM(rdir2)&4032)<<18);

uint32_cu dtype3= (dtype2==dtype1)?type1:dtype2;
if(dtype3<2){
typevec|= (typevec>>1);
typevec|= (typevec>>2);
typevec|= (typevec>>2);
typevec&= 0x41041041;
dtype3= 1;
}

if(((newinner&CBMASK)==bonded_before)&&((typevec&63)==dtype3)){dir2= 0;}
if((((newouter>>22)&CBMASK)==bonded_before)&&(((typevec>>6)&63)==dtype3)){dir2= 1;}
if((((newinner>>2)&CBMASK)==bonded_before)&&(((typevec>>12)&63)==dtype3)){dir2= 2;}
if((((newinner>>22)&CBMASK)==bonded_before)&&(((typevec>>18)&63)==dtype3)){dir2= 10;}
if((((newouter>>20)&CBMASK)==bonded_before)&&(((typevec>>24)&63)==dtype3)){dir2= 11;}
}

/*:15*/
#line 372 "./monolith.w"


if(dir2>=0){
/*16:*/
#line 466 "./monolith.w"



rdir2= dir2+(rdir*2);
rdir2= (rdir2>=12)?(rdir2-12):rdir2;
if(dir2==0){rdir2= 12;}


uint32_cu type2= (SPIRATOM(rdir2)>>6)&63;


bool valid= (type1> 1)&&(type2> 1);

if(valid){

uint64_cu state2= (SPIRATOM(rdir2)>>12);
uint64_cu state1= (SPIRATOM(rdir*2)>>12);

#define VALIDATE_REACTION
#include"loadrule.inc"
#undef VALIDATE_REACTION

if(valid){



SPIRATOM(rdir*2)&= 4095;
SPIRATOM(rdir*2)|= (state1<<12);
SPIRATOM(rdir2)&= 4095;
SPIRATOM(rdir2)|= (state2<<12);


uint32_cu xorbond= bonded_before^bonded_after;
if(dir2&9){xorbond<<= 20;}
if((dir2^(dir2>>1))&1){xorbond<<= 2;}
if(dir2&1){newouter^= xorbond;}else{newinner^= xorbond;}
}
}


/*:16*/
#line 375 "./monolith.w"

}

/*13:*/
#line 391 "./monolith.w"


newinner= ROTL24(newinner,4*rdir)&0xffffff;
newouter= ROTL24(newouter,4*rdir)&0xffffff;

/*:13*/
#line 378 "./monolith.w"

}
}

/*:10*/
#line 20 "./monolith.w"

/*17:*/
#line 518 "./monolith.w"


uint32_cu dir= H&0xffffff;
dir= dir%6;
dir= (((SPIRATOM(dir*2)&4032)|(ROTR24(newinner,4*dir)&563328))==0)?dir:dir+1;
dir= (dir> 5)?0:dir;
dir= (((SPIRATOM(dir*2)&4032)|(ROTR24(newinner,4*dir)&563328))==0)?dir:dir+1;
dir= (dir> 5)?0:dir;
dir= (((SPIRATOM(dir*2)&4032)|(ROTR24(newinner,4*dir)&563328))==0)?dir:dir+1;
dir= (dir> 5)?0:dir;
dir= (((SPIRATOM(dir*2)&4032)|(ROTR24(newinner,4*dir)&563328))==0)?dir:dir+1;
dir= (dir> 5)?0:dir;
dir= (((SPIRATOM(dir*2)&4032)|(ROTR24(newinner,4*dir)&563328))==0)?dir:dir+1;
dir= (dir> 5)?0:dir;

if(dir<6){
if((SPIRATOM(dir*2)&4032)==0){
if((SPIRATOM(12)&3968)!=0){



newinner= ROTR24(newinner,4*dir)&0xffffff;
newouter= ROTR24(newouter,4*dir)&0xffffff;

#ifdef CROSSBONDS
uint32_cu badbonds= 0;
#else
uint32_cu badbonds= (newinner>>1)&((newinner>>8)|(newinner>>16))&1;
#endif

badbonds|= (newinner&563328);

if(badbonds==0){

uint64_cu xordiff= SPIRATOM(2*dir)^SPIRATOM(12);
xordiff&= 0xffffffffffffffc0ull;
SPIRATOM(2*dir)^= xordiff;
SPIRATOM(12)^= xordiff;


newinner|= ((newinner&0x010000)<<5);
newinner|= ((newinner&0x100000)<<2);
newouter|= ((newinner&0x800000)>>3);
newinner|= ((newinner&0x000100)>>3);
newinner|= ((newinner&0x000010)>>2);
newouter|= ((newinner&0x000008)<<19);
newinner&= 0x6efee7;
}

newinner= ROTL24(newinner,4*dir)&0xffffff;
newouter= ROTL24(newouter,4*dir)&0xffffff;
}
}
}/*:17*/
#line 21 "./monolith.w"


/*8:*/
#line 269 "./monolith.w"



if((SPIRATOM(12)&4032)==0){
SPIRATOM(12)^= (H&0xfffffffffffff000ull);
}

/*:8*/
#line 23 "./monolith.w"

/*6:*/
#line 176 "./monolith.w"


outerbonds^= newouter;
SPIRATOM(2)^= (outerbonds&15);
SPIRATOM(3)^= (outerbonds&48);
outerbonds= outerbonds>>6;
SPIRATOM(5)^= (outerbonds&3);
SPIRATOM(6)^= (outerbonds&60);
outerbonds= outerbonds>>6;
SPIRATOM(7)^= (outerbonds&3);
SPIRATOM(9)^= (outerbonds&12);
SPIRATOM(10)^= (outerbonds&48);
outerbonds= outerbonds>>6;
SPIRATOM(10)^= (outerbonds&3);
SPIRATOM(11)^= (outerbonds&12);
SPIRATOM(1)^= (outerbonds&48);

innerbonds^= newinner;
SPIRATOM(12)^= (innerbonds&3);
SPIRATOM(0)^= (innerbonds&12);
SPIRATOM(2)^= (innerbonds&48);
innerbonds= innerbonds>>6;
SPIRATOM(4)^= (innerbonds&3);
SPIRATOM(12)^= (innerbonds&12);
SPIRATOM(4)^= (innerbonds&48);
innerbonds= innerbonds>>6;
SPIRATOM(6)^= (innerbonds&3);
SPIRATOM(8)^= (innerbonds&12);
SPIRATOM(12)^= (innerbonds&48);
innerbonds= innerbonds>>6;
SPIRATOM(8)^= (innerbonds&3);
SPIRATOM(10)^= (innerbonds&12);
SPIRATOM(0)^= (innerbonds&48);

/*:6*/
#line 24 "./monolith.w"

/*4:*/
#line 106 "./monolith.w"


A[y2][x3]= SPIRATOM(0);
A[y3][x4]= SPIRATOM(1);
A[y3][x3]= SPIRATOM(2);
A[y4][x3]= SPIRATOM(3);
A[y3][x2]= SPIRATOM(4);
A[y3][x1]= SPIRATOM(5);
A[y2][x1]= SPIRATOM(6);
A[y1][x0]= SPIRATOM(7);
A[y1][x1]= SPIRATOM(8);
A[y0][x1]= SPIRATOM(9);
A[y1][x2]= SPIRATOM(10);
A[y1][x3]= SPIRATOM(11);
A[y2][x2]= SPIRATOM(12);

/*:4*/
#line 25 "./monolith.w"


}

/*:1*/
