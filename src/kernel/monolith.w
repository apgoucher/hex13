@ CUDA kernel for artificial chemistry

The |hex13| kernel is applied to a cluster of 13 atom sites. At any
given tick, these clusters tessellate the entire lattice being simulated.

We take two parameters, namely a pointer to the torus and an `offset'
$o \in [0, 13)$, which determines which of the 13 possible tessellations
of clusters is used. These differ from each other by a simple translation.

@(monolith.h@>=
__global__ void doTick(uint64_cu A[GRIDSIZE][GRIDSIZE], uint32_cu offset) {

@<Compute coordinates of cluster@> @;

@<Load cluster into spiral array@> @;
@<Load bond data@> @;
@<Compute hash function@> @;

@<Obtain reaction from nearby enzyme@> @;
@<Find atoms and apply reaction@> @;
@<Apply physics@> @;

@<Save pseudorandom hash@> @;
@<Save bond data@> @;
@<Save spiral array into torus@> @;

}

@ Loading the data from the torus into shared memory

There are two good reasons for copying the data from the global torus
array into thread-local memory. Firstly, from a simplicity perspective
it means that we need not keep track of the coordinates $x_0 + \omega y_0$
of a distinguished atom in the cluster. Secondly, thread-local memory is
faster to access than global memory, so we keep global memory accesses
to an absolute minimum.

The first task is to determine the coordinates $x_0$ and $y_0$ of the
one of the atom-site. The grid of clusters is a scaled and rotated copy of
the Eisenstein integers, and we can convert a cluster coordinate $(i, j)$
into the position $x_0 + \omega y_0$ of its central atom by means of a
complex multiplication:

$$ x_0 + \omega y_0 = (i - j \omega^2)(4 + \omega) + o $$

This computation is performed componentwise:

@<Compute coordinates of cluster@>=

    uint32_cu prei = blockIdx.x * TBSIZE + threadIdx.x;
    uint32_cu prej = blockIdx.y * TBSIZE + threadIdx.y;
    uint32_cu x0 = (prei * 4 + prej * 3 + offset) % GRIDSIZE;
    uint32_cu y0 = (prei + prej * 4) % GRIDSIZE;

/*
The cluster is supported on five rows and five columns of the array. If
$x_0$ and $y_0$ are the lowest (smallest index) column and row, respectively,
then we compute the other coordinates by incrementation. Recalling that we
are on a torus, these are modular increments implemented as predicated
instructions.
*/

    uint32_cu x1 = (x0 == (GRIDSIZE - 1)) ? 0 : x0 + 1;
    uint32_cu y1 = (y0 == (GRIDSIZE - 1)) ? 0 : y0 + 1;
    uint32_cu x2 = (x1 == (GRIDSIZE - 1)) ? 0 : x1 + 1;
    uint32_cu y2 = (y1 == (GRIDSIZE - 1)) ? 0 : y1 + 1;
    uint32_cu x3 = (x2 == (GRIDSIZE - 1)) ? 0 : x2 + 1;
    uint32_cu y3 = (y2 == (GRIDSIZE - 1)) ? 0 : y2 + 1;
    uint32_cu x4 = (x3 == (GRIDSIZE - 1)) ? 0 : x3 + 1;
    uint32_cu y4 = (y3 == (GRIDSIZE - 1)) ? 0 : y3 + 1;

@ Shared memory.

Note that CUDA thread-local registers cannot be used for dynamically indexed
arrays. Consequently, we need to use shared memory (local to a thread block)
to emulate thread-local memory. This approach was advocated in the article:

https://devblogs.nvidia.com/parallelforall/fast-dynamic-indexing-private-arrays-cuda/

To abstract away this machinery, we define a macro |SPIRATOM| which
is indexed by an inward spiral: indices $0, \dots, 11$ refer to the twelve
non-central atoms in cyclic order, and index $12$ is the central atom.

@<Load cluster into spiral array@>=

    __shared__ uint64_cu B[13][TBSIZE][TBSIZE];
    #define SPIRATOM(X) B[X][threadIdx.y][threadIdx.x]

    SPIRATOM(0) = A[y2][x3];
    SPIRATOM(1) = A[y3][x4];
    SPIRATOM(2) = A[y3][x3];
    SPIRATOM(3) = A[y4][x3];
    SPIRATOM(4) = A[y3][x2];
    SPIRATOM(5) = A[y3][x1];
    SPIRATOM(6) = A[y2][x1];
    SPIRATOM(7) = A[y1][x0];
    SPIRATOM(8) = A[y1][x1];
    SPIRATOM(9) = A[y0][x1];
    SPIRATOM(10) = A[y1][x2];
    SPIRATOM(11) = A[y1][x3];
    SPIRATOM(12) = A[y2][x2];
 
@ We need two dual routines, one for loading the spiral array at the
beginning and another for saving it back into the torus at the end.

@<Save spiral array into torus@>=

    A[y2][x3] = SPIRATOM(0);
    A[y3][x4] = SPIRATOM(1);
    A[y3][x3] = SPIRATOM(2);
    A[y4][x3] = SPIRATOM(3);
    A[y3][x2] = SPIRATOM(4);
    A[y3][x1] = SPIRATOM(5);
    A[y2][x1] = SPIRATOM(6);
    A[y1][x0] = SPIRATOM(7);
    A[y1][x1] = SPIRATOM(8);
    A[y0][x1] = SPIRATOM(9);
    A[y1][x2] = SPIRATOM(10);
    A[y1][x3] = SPIRATOM(11);
    A[y2][x2] = SPIRATOM(12);

@ Separating bond data from atom data.

Bonds are conceptually distinct from atoms, and occupy spaces between atoms.
We can think of the molecules as graphs, whose vertices are atoms and edges
are bonds. However, for compactness, both bond and atom data is compressed
into the same array of 64-bit unsigned integers: each atom-site $z$ `owns'
the three bond-sites $z + {1 \over 2}$, $z + {1 \over 2} \omega$, and
$z + {1 \over 2} \omega^2$.

Whilst ideal for storage, this is inconvenient for operating upon. Hence,
we need a pair of routines to unpack the bond data and (after modification)
reinsert it into the lowest 6 bits of the 64-bit atom-site integers.


@<Load bond data@>=

    uint32_cu innerbonds = 0;
    innerbonds |= SPIRATOM(0) & 48;
    innerbonds |= SPIRATOM(10) & 12;
    innerbonds |= SPIRATOM(8) & 3;
    innerbonds = innerbonds << 6;
    innerbonds |= SPIRATOM(12) & 48;
    innerbonds |= SPIRATOM(8) & 12;
    innerbonds |= SPIRATOM(6) & 3;
    innerbonds = innerbonds << 6;
    innerbonds |= SPIRATOM(4) & 48;
    innerbonds |= SPIRATOM(12) & 12;
    innerbonds |= SPIRATOM(4) & 3;
    innerbonds = innerbonds << 6;
    innerbonds |= SPIRATOM(2) & 48;
    innerbonds |= SPIRATOM(0) & 12;
    innerbonds |= SPIRATOM(12) & 3;
    uint32_cu newinner = innerbonds;
    uint32_cu outerbonds = 0;
    outerbonds |= SPIRATOM(1) & 48;
    outerbonds |= SPIRATOM(11) & 12;
    outerbonds |= SPIRATOM(10) & 3;
    outerbonds = outerbonds << 6;
    outerbonds |= SPIRATOM(10) & 48;
    outerbonds |= SPIRATOM(9) & 12;
    outerbonds |= SPIRATOM(7) & 3;
    outerbonds = outerbonds << 6;
    outerbonds |= SPIRATOM(6) & 60;
    outerbonds |= SPIRATOM(5) & 3;
    outerbonds = outerbonds << 6;
    outerbonds |= SPIRATOM(3) & 48;
    outerbonds |= SPIRATOM(2) & 15;
    uint32_cu newouter = outerbonds;

@ Saving the bond data.

Reinsertion is managed by keeping the old bond data and computing a diff using
the XOR operation; this diff is applied by XORing it back into the array.

@<Save bond data@>=

    outerbonds ^= newouter;
    SPIRATOM(2) ^= (outerbonds & 15);
    SPIRATOM(3) ^= (outerbonds & 48);
    outerbonds = outerbonds >> 6;
    SPIRATOM(5) ^= (outerbonds & 3);
    SPIRATOM(6) ^= (outerbonds & 60);
    outerbonds = outerbonds >> 6;
    SPIRATOM(7) ^= (outerbonds & 3);
    SPIRATOM(9) ^= (outerbonds & 12);
    SPIRATOM(10) ^= (outerbonds & 48);
    outerbonds = outerbonds >> 6;
    SPIRATOM(10) ^= (outerbonds & 3);
    SPIRATOM(11) ^= (outerbonds & 12);
    SPIRATOM(1) ^= (outerbonds & 48);

    innerbonds ^= newinner;
    SPIRATOM(12) ^= (innerbonds & 3);
    SPIRATOM(0) ^= (innerbonds & 12);
    SPIRATOM(2) ^= (innerbonds & 48);
    innerbonds = innerbonds >> 6;
    SPIRATOM(4) ^= (innerbonds & 3);
    SPIRATOM(12) ^= (innerbonds & 12);
    SPIRATOM(4) ^= (innerbonds & 48);
    innerbonds = innerbonds >> 6;
    SPIRATOM(6) ^= (innerbonds & 3);
    SPIRATOM(8) ^= (innerbonds & 12);
    SPIRATOM(12) ^= (innerbonds & 48);
    innerbonds = innerbonds >> 6;
    SPIRATOM(8) ^= (innerbonds & 3);
    SPIRATOM(10) ^= (innerbonds & 12);
    SPIRATOM(0) ^= (innerbonds & 48);

@ Hash function.

We want atoms to diffuse randomly on the grid, but this is impossible in a
deterministic cellular automaton. Our workaround is to take a pseudorandom
hash of the |SPIRATOM| array, inspired by MurmurHash. This 64-bit
hash $H$ is our source of randomness in the kernel.

@<Compute hash function@>=

    uint64_cu H = SPIRATOM(12) + 57885161;

{

    uint64_cu h1 = SPIRATOM(1) ^ SPIRATOM(5) ^ SPIRATOM(9);
    uint64_cu h2 = SPIRATOM(3) ^ SPIRATOM(7) ^ SPIRATOM(11);

    uint64_cu k1 = SPIRATOM(0);
    uint64_cu k2 = SPIRATOM(2);
    k1 *= 0x87c37b91114253d5ull; k1  = ROTL64(k1,31); k1 *= 0x4cf5ad432745937full; h1 ^= k1;
    h1 = ROTL64(h1,27); h1 += h2; h1 = h1*5+0x52dce729ull;
    k2 *= 0x4cf5ad432745937full; k2  = ROTL64(k2,33); k2 *= 0x87c37b91114253d5ull; h2 ^= k2;
    h2 = ROTL64(h2,31); h2 += h1; h2 = h2*5+0x38495ab5ull;

    k1 = SPIRATOM(4);
    k2 = SPIRATOM(6);
    k1 *= 0x87c37b91114253d5ull; k1  = ROTL64(k1,31); k1 *= 0x4cf5ad432745937full; h1 ^= k1;
    h1 = ROTL64(h1,27); h1 += h2; h1 = h1*5+0x52dce729ull;
    k2 *= 0x4cf5ad432745937full; k2  = ROTL64(k2,33); k2 *= 0x87c37b91114253d5ull; h2 ^= k2;
    h2 = ROTL64(h2,31); h2 += h1; h2 = h2*5+0x38495ab5ull;

    k1 = SPIRATOM(8);
    k2 = SPIRATOM(10);
    k1 *= 0x87c37b91114253d5ull; k1  = ROTL64(k1,31); k1 *= 0x4cf5ad432745937full; h1 ^= k1;
    h1 = ROTL64(h1,27); h1 += h2; h1 = h1*5+0x52dce729ull;
    k2 *= 0x4cf5ad432745937full; k2  = ROTL64(k2,33); k2 *= 0x87c37b91114253d5ull; h2 ^= k2;
    h2 = ROTL64(h2,31); h2 += h1; h2 = h2*5+0x38495ab5ull;

    h1 ^= h1 >> 33;
    h1 *= 0xff51afd7ed558ccdull;
    h1 ^= h1 >> 33;
    h1 *= 0xc4ceb9fe1a85ec53ull;
    h1 ^= h1 >> 33;

    h2 ^= h2 >> 33;
    h2 *= 0xff51afd7ed558ccdull;
    h2 ^= h2 >> 33;
    h2 *= 0xc4ceb9fe1a85ec53ull;
    h2 ^= h2 >> 33;

    H += (h1 ^ h2);
}

@ Saving the hash.

If the central atom-site is empty (contains no atom), then there are 52
unused bits (which would store the atom's state). We take advantage of these
bits to store part of our hash, so that the empty space behaves as a massive
pseudorandom number generator.

@<Save pseudorandom hash@>=

    // Recycle random information if middle cell is empty:
    if ((SPIRATOM(12) & 4032) == 0) {
        SPIRATOM(12) ^= (H & 0xfffffffffffff000ull);
    }

@ Enzymes

Rather than having hardcoded chemical reactions in the rule, the reaction
to apply is decoded (in a custom manner) from the state of special atoms
called {\it enzymes}. The decoder and characterisation of enzymes are given
in the rule file, so we need to include |loadrule.inc| here.

The type of an atom is a positive integer in the interval $[2, 63]$, with
the types $0$ and $1$ representing empty space and immovable wall,
respectively. (In a reaction, the same bit-patterns $0$ and $1$ are used as
metasyntactic variables for representing arbitrary atom types.)

$$
{\rm atom type } =
\cases{
{\rm empty space if } n = 0; \cr
{\rm immovable wall if } n = 1; \cr
(x,y,e,f,a,b,c,d,g,h,i,j,\dots)[n] {\rm otherwise.} \cr
}
$$

@<Obtain reaction from nearby enzyme@>=

#define DEFINE_ENZYMES
#include "loadrule.inc"
#undef DEFINE_ENZYMES

/*
Quite confusingly, |REACTION_BITS| stores the number of bits that are
{\it not} used to encode the reaction. Right-shifting the 64-bit atom site
information by this amount yields the reaction number (typically 52 bits).
*/

#ifndef REACTION_BITS
#define REACTION_BITS 12
#endif

#define UPDATE_REACTION(X) r2 = SPIRATOM(X); r2 = (IS_ENZYME((r2 >> 6))) ? r2 : 0; reaction = (r2 >= (1 << REACTION_BITS)) ? r2 : reaction

uint64_cu reaction = 0;
{

    /*
    Load the highest-priority reaction atom into a register. More
    distant sites appear later in the list, therefore have higher
    priority (since we overwrite the reaction value). We check
    that an atom is of type `h' and non-zero state:
    */
    uint64_cu r2;

    UPDATE_REACTION(12);
    UPDATE_REACTION(0);
    UPDATE_REACTION(6);
    UPDATE_REACTION(10);
    UPDATE_REACTION(4);
    UPDATE_REACTION(8);
    UPDATE_REACTION(2);
    UPDATE_REACTION(1);
    UPDATE_REACTION(7);
    UPDATE_REACTION(11);
    UPDATE_REACTION(5);
    UPDATE_REACTION(9);
    UPDATE_REACTION(3);

    // The upper 52 bits are the interesting ones:
    reaction = reaction >> REACTION_BITS;
}

@ Finding atoms compatible with the reaction

Now that we have a potential reaction to apply, we look for a compatible
pair of atoms to which to apply the reaction. For coding simplicity, we
assume that the first atom is in one of the 6 atom-sites neighbouring
the central atom-site, and the second atom is in one of the 5 atom-sites
neighbouring the first atom-site.

We exploit the rotational symmetry of the cluster to assume without loss
of generality that the first atom points to the east. This involves some
cyclic shifts of the 24-bit bond registers.

If bonds are not allowed to cross, we also check that the pair of atoms
do not react transversally `through' an existing bond.

@<Find atoms and apply reaction@>=

    if (reaction != 0) {

        #define DECODE_REACTION
        #include "loadrule.inc"
        #undef DECODE_REACTION

        @<Find first atom@> @;
        if (rdir < 6) {

            @<Rotate bonds so first atom points east@> @;
            @<Get first atom type@> @;
            @<Find second atom@> @;

            if (dir2 >= 0) {
                @<Apply reaction to specific atoms@> @;
            }

            @<Unrotate bonds@> @;
        }
    }

@ @<Get first atom type@>=

    uint32_cu type1 = (SPIRATOM(rdir * 2) >> 6) & 63;

@ @<Rotate bonds...@>=

    newinner = ROTR24(newinner, 4 * rdir) & 0xffffff;
    newouter = ROTR24(newouter, 4 * rdir) & 0xffffff;

@ @<Unrotate bonds@>=

    newinner = ROTL24(newinner, 4 * rdir) & 0xffffff;
    newouter = ROTL24(newouter, 4 * rdir) & 0xffffff;

@ @<Find first atom@>=

    #define CHECK_FIRST_ATOM
    #include "loadrule.inc"
    #undef CHECK_FIRST_ATOM

    // Choose a random initial direction for the reaction to apply.
    // We do this well in advance to give time for the modulus result
    // to return from the ALU:
    uint32_cu rdir = (H >> 7) & 0xffffff;
    rdir = rdir % 6;

    // If the reaction obviously doesn't apply, rotate by 60 degrees and try again:
    rdir = (VERIFY_ATOM(SPIRATOM(rdir * 2))) ? rdir : rdir + 1;
    rdir = (rdir > 5) ? 0 : rdir;
    rdir = (VERIFY_ATOM(SPIRATOM(rdir * 2))) ? rdir : rdir + 1;
    rdir = (rdir > 5) ? 0 : rdir;
    rdir = (VERIFY_ATOM(SPIRATOM(rdir * 2))) ? rdir : rdir + 1;
    rdir = (rdir > 5) ? 0 : rdir;
    rdir = (VERIFY_ATOM(SPIRATOM(rdir * 2))) ? rdir : rdir + 1;
    rdir = (rdir > 5) ? 0 : rdir;
    rdir = (VERIFY_ATOM(SPIRATOM(rdir * 2))) ? rdir : rdir + 1;
    rdir = (rdir > 5) ? 0 : rdir;
    rdir = (VERIFY_ATOM(SPIRATOM(rdir * 2))) ? rdir : 6;

@ @<Find second atom@>=

    #ifdef CROSSBONDS
    #define CBMASK 1
    #else
    #define CBMASK 3
    #endif

    // Find a neighbouring site with the correct bondedness.
    // This implicitly incorporates a check that bonds will not cross
    // after applying the reaction:
    int dir2 = -1;
    int rdir2 = rdir * 2 + 1;
    {
        uint32_cu typevec = (SPIRATOM(12) & 4032) >> 6;
        typevec |= (SPIRATOM(rdir2) & 4032);
        rdir2 = (rdir2 >= 11) ? (rdir2 - 11) : (rdir2 + 1);
        typevec |= ((SPIRATOM(rdir2) & 4032) << 6);
        rdir2 = (rdir2 >= 4) ? (rdir2 - 4) : (rdir2 + 8);
        typevec |= ((SPIRATOM(rdir2) & 4032) << 12);
        rdir2 = (rdir2 >= 11) ? (rdir2 - 11) : (rdir2 + 1);
        typevec |= ((SPIRATOM(rdir2) & 4032) << 18);

        uint32_cu dtype3 = (dtype2 == dtype1) ? type1 : dtype2;
        if (dtype3 < 2) {
            typevec |= (typevec >> 1);
            typevec |= (typevec >> 2);
            typevec |= (typevec >> 2);
            typevec &= 0x41041041;
            dtype3 = 1;
        }

        if (((newinner         & CBMASK) == bonded_before) && ((typevec & 63) == dtype3)) { dir2 = 0; }
        if ((((newouter >> 22) & CBMASK) == bonded_before) && (((typevec >> 6) & 63) == dtype3)) { dir2 = 1; }
        if ((((newinner >>  2) & CBMASK) == bonded_before) && (((typevec >> 12) & 63) == dtype3)) { dir2 = 2; }
        if ((((newinner >> 22) & CBMASK) == bonded_before) && (((typevec >> 18) & 63) == dtype3)) { dir2 = 10; }
        if ((((newouter >> 20) & CBMASK) == bonded_before) && (((typevec >> 24) & 63) == dtype3)) { dir2 = 11; }
    }

@ Applying the reaction

Assuming we have a pair of probably-compatible atoms, we now verify for
certain that they are actually compatible. If so, the reaction can be
applied to modify the atoms and bonds in question.

@<Apply reaction to specific atoms@>=

    // Find the index to the correct atom:
    rdir2 = dir2 + (rdir * 2);
    rdir2 = (rdir2 >= 12) ? (rdir2 - 12) : rdir2;
    if (dir2 == 0) { rdir2 = 12; }

    // Actual atom types:
    uint32_cu type2 = (SPIRATOM(rdir2) >> 6) & 63;

    // Check that both reactants are atoms (rather than empty space/walls):
    bool valid = (type1 > 1) && (type2 > 1);

    if (valid) {
        // Atom types are suitable:
        uint64_cu state2 = (SPIRATOM(rdir2) >> 12);
        uint64_cu state1 = (SPIRATOM(rdir * 2) >> 12);

        #define VALIDATE_REACTION
        #include "loadrule.inc"
        #undef VALIDATE_REACTION

        if (valid) {
            // The reaction actually applies. Excellent.

            // Update atom states:
            SPIRATOM(rdir * 2) &= 4095;
            SPIRATOM(rdir * 2) |= (state1 << 12);
            SPIRATOM(rdir2) &= 4095;
            SPIRATOM(rdir2) |= (state2 << 12);

            // Make or break bonds as necessary:
            uint32_cu xorbond = bonded_before ^ bonded_after;
            if (dir2 & 9) { xorbond <<= 20; }
            if ((dir2 ^ (dir2 >> 1)) & 1) { xorbond <<= 2; }
            if (dir2 & 1) { newouter ^= xorbond; } else { newinner ^= xorbond; }
        }
    }


@ Physics

So far, we have dealt with the {\it chemistry} of how atoms can react
(change state and molecular graph). This leaves the {\it physics}, which
here refers to the diffusive movement of atoms subject to mechanical
constraints.

The pseudorandom hash $H$ again determines a random direction, and we
successively try the 6 possible directions to move the central atom (if it
exists). The magic number $563328$ refers to the pattern of inner bonds
which would prevent the movement of the atom.

@<Apply physics@>=

    uint32_cu dir = H & 0xffffff;
    dir = dir % 6;
    dir = (((SPIRATOM(dir * 2) & 4032) | (ROTR24(newinner, 4 * dir) & 563328)) == 0) ? dir : dir + 1;
    dir = (dir > 5) ? 0 : dir;
    dir = (((SPIRATOM(dir * 2) & 4032) | (ROTR24(newinner, 4 * dir) & 563328)) == 0) ? dir : dir + 1;
    dir = (dir > 5) ? 0 : dir;
    dir = (((SPIRATOM(dir * 2) & 4032) | (ROTR24(newinner, 4 * dir) & 563328)) == 0) ? dir : dir + 1;
    dir = (dir > 5) ? 0 : dir;
    dir = (((SPIRATOM(dir * 2) & 4032) | (ROTR24(newinner, 4 * dir) & 563328)) == 0) ? dir : dir + 1;
    dir = (dir > 5) ? 0 : dir;
    dir = (((SPIRATOM(dir * 2) & 4032) | (ROTR24(newinner, 4 * dir) & 563328)) == 0) ? dir : dir + 1;
    dir = (dir > 5) ? 0 : dir;

    if (dir < 6) {
        if ((SPIRATOM(dir * 2) & 4032) == 0) {
            if (( SPIRATOM(12) & 3968) != 0) {
                // The central cell contains an atom, and the other cell is
                // empty. We shall check bond fidelity before attempting to
                // swap the cell contents.
                newinner = ROTR24(newinner, 4 * dir) & 0xffffff;
                newouter = ROTR24(newouter, 4 * dir) & 0xffffff;

                #ifdef CROSSBONDS
                uint32_cu badbonds = 0;
                #else
                uint32_cu badbonds = (newinner >> 1) & ((newinner >> 8) | (newinner >> 16)) & 1;
                #endif

                badbonds |= (newinner & 563328);

                if (badbonds == 0) {
                    // We can legally move the atom without causing bonds to cross:
                    uint64_cu xordiff = SPIRATOM(2 * dir) ^ SPIRATOM(12);
                    xordiff &= 0xffffffffffffffc0ull;
                    SPIRATOM(2 * dir) ^= xordiff;
                    SPIRATOM(12) ^= xordiff;

                    // We have to move some bonds as well:
                    newinner |= ((newinner & 0x010000) << 5);
                    newinner |= ((newinner & 0x100000) << 2);
                    newouter |= ((newinner & 0x800000) >> 3);
                    newinner |= ((newinner & 0x000100) >> 3);
                    newinner |= ((newinner & 0x000010) >> 2);
                    newouter |= ((newinner & 0x000008) << 19);
                    newinner &= 0x6efee7;
                }

                newinner = ROTL24(newinner, 4 * dir) & 0xffffff;
                newouter = ROTL24(newouter, 4 * dir) & 0xffffff;
            }
        }
    }
