The speed of hex13 is reliant on having a single, computation-heavy
kernel that performs all of the physics and chemistry updates for a
cluster of 13 adjacent sites. Originally this was all contained in
hex13.cu, but it is cleaner to refactor it into individual files
which are #included into the body of the kernel declaration.
