#include "webserver.h"

int main(int argc, char* argv[]) {

    std::cerr << "\033[33;1m***** Welcome to hex13 *****\033[0m" << std::endl << std::endl;

    std::cerr << "Documentation can be found in README.md, which is available online at" << std::endl;
    std::cerr << "https://gitlab.com/apgoucher/hex13 together with the full source code." << std::endl;
    std::cerr << "This project was created by Adam P. Goucher based on earlier research" << std::endl;
    std::cerr << "by Tim Hutton. If you discover anything exciting, or have any ideas" << std::endl;
    std::cerr << "about how to improve hex13, you are encouraged to contact us. Please" << std::endl;
    std::cerr << "wait a few seconds while the simulator loads..." << std::endl << std::endl;

    std::cerr << "[      ] Load preferences\r";

    // Load default values from preferences file:
    std::string foreground = DEFAULT_FOREGROUND;
    std::string background = DEFAULT_BACKGROUND;
    double density = DEFAULT_DENSITY;

    std::cerr << "[  \033[32;1mOK\033[0m  ] Load preferences\n";
    std::cerr << "[      ] Read command-line arguments\r";

    // Parse command-line arguments:
    for (int i = 1; i < argc; i++) {
        std::string argument = argv[i];
        if ((argument == "-f") || (argument == "--foreground")) {
            foreground = argv[i+1];
        } else if ((argument == "-b") || (argument == "--background")) {
            background = argv[i+1];
        } else if ((argument == "-d") || (argument == "--density")) {
            density = std::stod(argv[i+1]);
        }
    }

    std::cerr << "[  \033[32;1mOK\033[0m  ] Read command-line arguments\n" << std::endl;

    // Print preferences:
    std::cerr << "World rule: " << RULENAME << std::endl;
    std::cerr << "World size: " << GRIDSIZE << " x " << GRIDSIZE << std::endl;
    std::cerr << "Background: " << background << " (density " << density << ")" << std::endl;
    std::cerr << "Foreground: " << foreground << std::endl << std::endl;

    #ifdef CROSSBONDS
    std::cerr << "Bonds \033[32;1mare\033[0m allowed to cross." << std::endl << std::endl;
    #else
    std::cerr << "Bonds \033[31;1mare not\033[0m allowed to cross." << std::endl << std::endl;
    #endif

    // This manages communication with the GPU and is threadsafe:
    std::cerr << "[      ] Initialise mutex13world\r";
    mutex13world mw;
    std::cerr << "[  \033[32;1mOK\033[0m  ] Initialise mutex13world\n";

    // Create random background from file:
    std::cerr << "[      ] Load background\r";
    mw.loadBackground(background, density);
    std::cerr << "[  \033[32;1mOK\033[0m  ] Load background\n";

    // Load pattern file as foreground:
    std::cerr << "[      ] Load foreground\r";
    mw.loadPattern(foreground, std::pair<int64_t, int64_t>(30, 10));
    std::cerr << "[  \033[32;1mOK\033[0m  ] Load foreground\n";

    // Memoize the .html and .js files so we can serve them:
    std::cerr << "[      ] Load static pages\r";
    mw.loadfile("hex13.html");
    mw.loadfile("hex13.js");
    std::cerr << "[  \033[32;1mOK\033[0m  ] Load static pages\n";

    // Run the webserver on port 8888:
    std::cerr << "[      ] Initialise webserver\r";
    mhdserver mhs;
    mhs.start(&mw);
    std::cerr << "[  \033[32;1mOK\033[0m  ] Initialise webserver\n" << std::endl;

    // Run a timing loop to get synchronisation correct:
    int evocount = 100;

    // Do everything:
    while (true) {
        evocount = 1 + (mw.evolve(evocount) / 6);
    }

    return 0;

}
