#include "hex13.h"
#include <cstring>
#include <iostream>
#include <vector>

#define ROTR24(X, Y) (((X) >> (Y)) | ((X) << (24 - (Y))))
#define ROTL24(X, Y) (((X) << (Y)) | ((X) >> (24 - (Y))))
#define ROTR64(X, Y) (((X) >> (Y)) | ((X) << (64 - (Y))))
#define ROTL64(X, Y) (((X) << (Y)) | ((X) >> (64 - (Y))))

/*
Kernel which computes an entire tick (physics + chemistry) of the hex13
artificial chemistry. The reason for combining these into a single kernel
is to minimise the number of accesses to global memory (at the moment,
only 13 reads and 13 writes of 64-bit fields are performed).
*/

#include "kernel/monolith.h"

void doEpoch(uint64_cu A[GRIDSIZE][GRIDSIZE]) {

    dim3 threadsPerBlock(TBSIZE, TBSIZE);
    dim3 numBlocks(GRIDSIZE / TBSIZE, GRIDSIZE / (TBSIZE * 13));

    // we use a maximal-period linear recurrence sequence with repeated
    // elements removed in order to obtain a de Bruijn sequence of all
    // pairs (a, b) of distinct elements modulo 13:
    int x = 1; int y = 1; int ticks = 0;

    for (int i = 0; i < 168; i++) {

        // advance linear recurrence sequence:
        { int z = (y + 11 * x) % 13; x = y; y = z; }

        if (x == y) { continue; }

        doTick<<<numBlocks, threadsPerBlock>>>(A, x);
        cudaError_t err = cudaGetLastError();
        if (err != cudaSuccess) { std::cerr << "Error: " << cudaGetErrorString(err) << std::endl; }
        ticks += 1;
    }

    if ((x != 1) || (y != 1) || (ticks != 156)) {
        std::cerr << "Error: linear recurrence sequence is broken." << std::endl;
    }
}

hex13world::hex13world() {
    cudaMalloc((void**)&d_A, (GRIDSIZE * GRIDSIZE) * sizeof(uint64_cu));
    cudaMallocHost((void**)&h_A, (GRIDSIZE * GRIDSIZE) * sizeof(uint64_t));
    std::memset(h_A, 0, (GRIDSIZE * GRIDSIZE) * sizeof(uint64_t));
}

hex13world::~hex13world() {
    cudaFreeHost((void*)h_A);
    cudaFree((void*)d_A);
}

void hex13world::copyHostToDevice() {
    cudaMemcpy(d_A, h_A, (GRIDSIZE * GRIDSIZE) * sizeof(uint64_cu), cudaMemcpyHostToDevice);
        cudaError_t err = cudaGetLastError();
        if (err != cudaSuccess) { std::cerr << "Error: " << cudaGetErrorString(err) << std::endl; }
}

void hex13world::copyDeviceToHost() {
    cudaMemcpy(h_A, d_A, (GRIDSIZE * GRIDSIZE) * sizeof(uint64_cu), cudaMemcpyDeviceToHost);
        cudaError_t err = cudaGetLastError();
        if (err != cudaSuccess) { std::cerr << "Error: " << cudaGetErrorString(err) << std::endl; }
}

void hex13world::performEpoch() {
    doEpoch(d_A);
}

void hex13world::waitForDevice() {
    cudaDeviceSynchronize();
}
