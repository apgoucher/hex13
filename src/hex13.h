#pragma once

#include "../scripts/hex13prefs.py"
#include <stdint.h>
#include <vector>

// Specific integer types:
#define uint64_cu unsigned long long int
#define uint32_cu unsigned int

// Static assertions:
static_assert((GRIDSIZE % (TBSIZE * 13)) == 0,
    "GRIDSIZE must be divisible by 13 * TBSIZE");

static_assert(sizeof(uint64_cu) == sizeof(uint64_t),
    "uint64_cu must be an unsigned 64-bit integer");

static_assert(sizeof(uint32_cu) == sizeof(uint32_t),
    "uint32_cu must be an unsigned 32-bit integer");


class hex13world {

    private:
    // The CUDA device memory is private:
    uint64_cu (*d_A)[GRIDSIZE];

    public:
    // The host memory is public:
    uint64_t (*h_A)[GRIDSIZE];

    hex13world();
    ~hex13world();

    void copyHostToDevice();
    void copyDeviceToHost();
    void performEpoch();
    void waitForDevice();

};
