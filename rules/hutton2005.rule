// Enzyme definition:

#ifdef DEFINE_ENZYMES

// Any atom can be an enzyme:
#define IS_ENZYME(STATETYPE) (((STATETYPE) & 63) > 1)

// Bits 20 and above encode the reaction:
#define REACTION_BITS 20

#undef DEFINE_ENZYMES
#endif

// This routine must accept the following parameters:
// :param: 'reaction' -- 44-bit reaction integer
// and return the following:
// :return: 'dtype1' -- desired type of first atom
// :return: 'dtype2' -- desired type of second atom
// :return: 'dstate1' -- desired state of first atom
// :return: 'bonded_before' -- whether the atoms should be bonded to begin with (0 = unbonded; 1 = bonded)

#ifdef DECODE_REACTION

    uint32_cu dtype1 = (reaction >> 3) & 63;
    uint32_cu dtype2 = (reaction >> 9) & 63;

    uint32_cu dstate1 = (reaction >> 15) & 127;

    #define bonded_before (reaction & 1)

#undef DECODE_REACTION
#endif

// This routine must accept the following parameters:
// :param: 'dtype1' -- desired type of first atom
// :param: 'dstate1' -- desired type of second atom
// and define the following:
// :return: VERIFY_ATOM(DESCRIPTOR) -- function macro which accepts the 64-bit descriptor and evaluates it.

#ifdef CHECK_FIRST_ATOM

    uint32_cu mright = (dtype1 < 2) ? 12 : 6;
    uint32_cu mmatch = (dtype1 < 2) ? dstate1 : ((dstate1 << 6) | dtype1);

    // We only check the lowest 8 bits of the atom state, leaving the other
    // 44 bits for encoding a reaction.
    #define VERIFY_ATOM(DESCRIPTOR) (((DESCRIPTOR & 0xfffff) >> mright) == mmatch)

#undef CHECK_FIRST_ATOM
#endif

// This routine must accept the following parameters:
// :param: 'state1' -- initial state of first atom
// :param: 'state2' -- initial state of second atom
// :param: 'valid' -- boolean variable to overwrite
// :params: anything left over from section DECODE_REACTION
// and return the following:
// :return: 'state1' -- final state of first atom
// :return: 'state2' -- final state of second atom
// :return: 'bonded_after' -- whether the atoms should be bonded afterwards (0 = unbonded; 1 = bonded)
// :return: 'valid' -- whether to effect the reaction

#ifdef VALIDATE_REACTION

    valid = (((state2 ^ (reaction >> 22)) & 127) == 0);

    state1 = (state1 & 0xffffffffffffff00ull) | ((reaction >> 29) & 127);
    state2 = (reaction & 4) ? state1 : state2;
    state2 = (state2 & 0xffffffffffffff00ull) | ((reaction >> 36) & 127);

    #define bonded_after ((reaction >> 1) & 1)

#undef VALIDATE_REACTION
#endif
