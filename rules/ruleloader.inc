/*
* This file is called by src/kernel/loadrule.inc and loads whichever rule
* is specified in the preferences file. See linear.inc for an example of
* how to write a hex13 rule.
*/

#include RULENAME
