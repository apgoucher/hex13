#!/usr/bin/python

from reaction_parser import reactint

def r2genes(rl):

    return [reactint(r) for r in rl.split('\n') if ((len(r) > 0) and (r[0] != '#'))]


def drawgene(gene, alt):

    curline = [">3 g1 >0", ">3 g1 >6"][alt % 2]
    remgene = gene

    while (remgene > 0):
        nch = 'kbcd'[remgene % 4]
        remgene = remgene >> 2
        curline += (' %s1' % nch)

    return curline


seedgenes='''

# Membrane growth (4 reactions):
a0 + a12 --> a14a15
a15a13 --> a16a17
a17 + a14 --> a18a13
a16a18 --> a11 + a12

# Ribosome (5 reactions):
f25a8 --> f26 + a9
a9a11 --> a7 + a5
a7a6 --> a11a41
a41 + f(42+n) --> a(42+n)f25
h0 + a(42+n) --> h(0+n)a8

# Used for both template replication and ribosome:
x5 + x0 --> x7x6

# Attachment (13 reactions):

e8 + f0 --> e40f11

f11 + a11 --> f12a40
a40a11 --> a39a38
f12 + a38 --> f13a37
a37a11 --> a36a35
f13 + a35 --> f14a25
f14a36 --> f15 + a33

a39a11 --> a2a10
f15 + a10 --> f9a35
a2f9 --> a33 + f9
a33a33 --> a13a31
a31 + a0 --> a12a41

f9 + a41 --> f16 + a41

# Propagation (18 reactions):

x1e(42+2n) --> x(42+2n)e31
x1k(42+2n) --> x(42+8n)k31
x1b(42+2n) --> x(44+8n)b31
x1c(42+2n) --> x(46+8n)c31
x1d(42+2n) --> x(48+8n)d31

x31g(42+2n) --> x(42+2n)g30
g30x(42+2n) --> g(43+2n)x29
x29y31 --> x28y29
x28y(43+2n) --> x(43+2n)y31
g31x1 --> g31x42
f42x31 --> f1x27
x27y31 --> x1y27
f29e(43+2n) --> f(42+n)e31
f26e29 --> f29e28

f26e27 --> f41e40
f41a(33+n) --> f(17+n)a41
a41f(17+n) --> a(32+n)f16
f16e40 --> f31e42
'''

othergenes='''
# Replication (8 reactions):

e8 + e0 --> e4e3
x4 + y1 --> x2y5
x3 + y6 --> x2y10
x7y10 --> x4y3
f4f3 --> f8 + f8
x2y8 --> x9y1
x9y9 --> x8 + y8

# Mitotic spindle (18 reactions):

f41a32 --> f30a26
f30 + a41 --> f32 + a11

a26 + x40 --> a26 + x39
a25 + x39 --> a25 + x38
a26x32 --> a26 + x33
a25x33 --> a25 + x34
x34y38 --> x35y36
x36y1 --> x32y40
f36a26 --> f35a24
a25 + a24 --> a22a22
a22a11 --> a21a20
a21a20 --> a22a11
a20 + a20 --> a23a23
a23a21 --> a11 + a19
a19f35 --> a11 + f2
a19f32 --> a11 + f41
x41y36 --> x1y41
f41e1 --> f41 + e8
'''

def main():

    si = r2genes(seedgenes)
    ai = si + r2genes(othergenes)

    print("@40,10 f1")
    for (i, g) in enumerate(ai[::-1]):
        print(drawgene(g, i))
    print("e8")

    print("@-15,0")
    print(" ".join([">0"]  + 99 * ["a11"]))
    print(" ".join([">2"]  + 99 * ["a11"]))
    print(" ".join([">4"]  + 99 * ["a11"]))
    print(" ".join([">6"]  + 99 * ["a11"]))
    print(" ".join([">8"]  + 99 * ["a11"]))
    print(" ".join([">10"] + 99 * ["a11"]))
    print(" ".join([">0"]  + 99 * ["a11"]))

    for (i, g) in enumerate(si):
        i2 = len(si) - i
        for j in [2, 4, 6, 78, 80, 82]:
            print("@%d,%d" % (j + i + 5, 2*i + 10))
            print(">0 l7 h%s" % hex(g))
            print("@%d,%d" % (j + i2 + 5, 2*i2 + 11))
            print(">0 l7 h%s" % hex(g))

main()
