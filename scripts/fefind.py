#!/usr/bin/python

import json
from sys import argv, stdin

def tuplemod(t, m):

    return tuple((((i % m) + m) % m) for i in t)

def jsongraph(filename=None, diam=416):

    verts = {}

    with (open(filename) if (filename is not None) else stdin) as json_data:
        d = json.load(json_data)
        for x in d["atoms"]:
            verts[tuplemod(x["coords"], diam)] = [x["type"] + str(x["state"])]
        for x in d["bonds"]:
            y = map((lambda t : tuplemod(t, diam)), x)
            verts[y[0]].append(y[1])
            verts[y[1]].append(y[0])

    verts = {k : v for (k, v) in verts.iteritems() if (len(v) > 1)}

    return verts

def fefind(jg):

    for (fk, fv) in jg.iteritems():
        if (fv[0][0] == 'f'):
            for ek in fv[1:]:
                ev = jg[ek]
                if (ev[0][0] == 'e'):
                    h = [fv[0], ev[0]]
                    visited = {fk, ek}
                    pots = [i for i in ev[1:] if i not in visited]
                    while (len(pots) > 0):
                        ek = pots[0]
                        visited.add(ek)
                        ev = jg[ek]
                        h.append(ev[0])
                        pots = [i for i in ev[1:] if i not in visited]
                    print(h)

fefind(jsongraph(argv[1] if (len(argv) > 1) else None))
