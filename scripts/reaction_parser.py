#!/usr/bin/python

# Script for parsing reactions in human-readable form, e.g.
# x1b(42+n) --> x(43+4n)b31

from hex13prefs import prefdict
from itertools import takewhile

revdict = {c : i for (i, c) in enumerate(prefdict["ATOMTYPES"])}

def tokenise_reaction(l):

    fsm = 0
    atomtype = None
    atomstate = ""
    tokens = []

    i = 0
    while (i < len(l)):
        if l[i] in revdict:
            atomtype = revdict[l[i]]
            if (l[i+1] == '('):
                s = l[i+2:]
                s = s[:s.index(')')]
                i += (3 + len(s))
            else:
                s = l[i+1:]
                s = "".join(takewhile(str.isdigit, s))
                i += (1 + len(s))
            if ((len(tokens) % 2) == 1):
                tokens.append('-')
            s = s.replace(" ", "").replace("*", "").lower().split("+")
            cterm = 0
            lterm = 0
            for t in s:
                if (t[-1] == 'n'):
                    lterm += int(t[:-1] if (len(t) > 1) else 1)
                else:
                    cterm += int(t)
            tokens.append((atomtype, cterm, lterm))
        else:
            if ((l[i] in '+>') and ((len(tokens) % 2) == 1)):
                tokens.append(l[i])
            i += 1

    return tokens

def reactint(l, rule='linear'):

    tokens = tokenise_reaction(l)
    indices = [i for (i, t) in enumerate(tokens) if (t == '>')]

    if (len(indices) != 1):
        raise ValueError('Reaction %s should contain exactly one arrow "-->".' % l)
    elif (indices[0] != 3):
        raise ValueError('Reaction %s should have exactly two atoms on the left of the arrow.' % l)
    elif (len(tokens) != 7):
        raise ValueError('Reaction %s should have exactly two atoms on the right of the arrow.' % l)
    elif (tokens[0][0] != tokens[4][0]):
        raise ValueError('First and third atoms in %s have unequal types.' % l)
    elif (tokens[2][0] != tokens[6][0]):
        raise ValueError('Second and fourth atoms in %s have unequal types.' % l)
    elif (tokens[0][2] != 0):
        raise ValueError('First atom in %s must have constant state.' % l)
    elif ((tokens[6][2] != 0) and (rule == 'linear')):
        raise ValueError('Fourth atom in %s must have constant state.' % l)

    # At this point, we know the reaction is well-formed:
    a = tokens[0][1]
    b = tokens[2][1]
    c = tokens[4][1]
    d = tokens[6][1]
    e = tokens[2][2]
    f = tokens[4][2]
    g = tokens[6][2]

    if (rule == 'linear'):
        convdown = {0: 0, 1: 1, 2: 2, 4: 3, 8: 4, 16: 5, 32: 6, 64: 7}
    elif (rule == 'bilinear'):
        convdown = {0: 0, 1: 1, 2: 2, 4: 3}
    else:
        raise ValueError('Unrecognised rule: %s' % rule)

    if ((e not in convdown) or (f not in convdown) or (g not in convdown)):
        raise ValueError('Multipliers in %s must be in %s' % (l, str(sorted(convdown.keys))))
    elif ((a < 0) or (a > 255)):
        raise ValueError('First atom in %s has state outside [0, 255].' % l)
    elif ((b < 0) or (b > 255)):
        raise ValueError('Second atom in %s has state outside [0, 255].' % l)
    elif ((c < 0) or (c > 255)):
        raise ValueError('Third atom in %s has state outside [0, 255].' % l)
    elif ((d < 0) or (d > 255)):
        raise ValueError('Fourth atom in %s has state outside [0, 255].' % l)

    # We're off to the races:
    ri = (a & 15) | ((b & 15) << 4) | ((c & 15) << 8) | ((d & 15) << 12)
    ri |= (((a & 240) << 28) | ((b & 240) << 32) | ((c & 240) << 36) | ((d & 240) << 40))

    e = convdown[e]
    f = convdown[f]
    g = convdown[g]
    bb = (0 if (tokens[1] == '+') else 1)
    ba = (0 if (tokens[5] == '+') else 1)

    if (rule == 'linear'):
        ri |= ((bb << 20) | (ba << 21) | (e << 22) | (f << 25))
    elif (rule == 'bilinear'):
        ri |= ((bb << 20) | (ba << 21) | (e << 22) | (f << 24) | (g << 26))
    ri |= (((tokens[0][0] &  3) << 16) | ((tokens[2][0] &  3) << 18))
    ri |= (((tokens[0][0] & 12) << 26) | ((tokens[2][0] & 12) << 28))
    ri |= (((tokens[0][0] & 48) << 44) | ((tokens[2][0] & 48) << 46))

    return ri

def main():

    rule = 'linear'
    if ((len(argv) >= 3) and (argv[1] == '--rule')):
        rule = argv[2];
        reactions = argv[3:]
    else:
        reactions = argv[1:]

    print(' '.join(map(lambda x : hex(reactint(x, rule)), reactions)))

if (__name__ == '__main__'):

    from sys import argv
    main()
