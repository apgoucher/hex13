#pragma once
#ifndef ATOMTYPES /*
includeme=''''; python "$0" "$@"; exit $?

Feel free to modify the hex13 preferences displayed below, but do not
remove this explanation as it will prevent successful compilation. You can
freely use any form of literals which are valid in both C++ and Python,
along with bitshifts and the four common arithmetic operators: +-*/

// Rule to implement:
#define RULENAME "linear.rule"

// Square-root of number of hexagons in grid:
#define GRIDSIZE (13 << 6)

// Square-root of number of threads per block:
#define TBSIZE 8

// Uncomment this to allow bonds to cross:
// #define CROSSBONDS

// Default foreground/background/density:
#define DEFAULT_FOREGROUND "patterns/timrep.h13"
#define DEFAULT_BACKGROUND "backgrounds/replicase.bkg"
#define DEFAULT_DENSITY 0.22

// Atom types (please do not change this as it may break peoples' patterns):
#define ATOMTYPES "xyefabcdghijklmnopqrstuvwzXYEFABCDGHIJKLMNOPQRSTUVWZ0123456789_$"

// Atom type denoting enzymes (9 == 'h'):
#define ENZYME_TYPE 9

// '''; from parseprefs import *; from sys import stdout; stdout.write((str(prefdict) + "\n") if (__name__ == "__main__") else "")

#endif
