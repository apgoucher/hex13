#!/usr/bin/python

from hex13prefs import *

def medieval(s):
    if ('__' in s):
        return s
    try:
        a = eval(s, {'__builtins__':{}})
    except Exception:
        a = s
    return a

preflines = [x.split(None, 2) for x in includeme.split('\n')]
preflines = [c for c in preflines if ((len(c) >= 1) and (c[0][0] == '#'))]
prefdict = {c[1] : (medieval(c[2]) if (len(c) >= 3) else None) for c in preflines if (c[0] == '#define')}
