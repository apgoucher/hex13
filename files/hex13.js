var currentEpoch = 0;

var vpx = 0;
var vpy = 0;

var readyToReceive = true;
var skips = 0;

document.onkeydown = function(e) {
    if (!e) { e = window.event; }
    var code = e.keyCode;
    if (e.charCode && code == 0) { code = e.charCode; }
    switch (code) {
        case 37:
            // alert('left');
            vpx -= 2;
            break;
        case 38:
            // alert('up');
            vpy -= 2;
            vpx -= 1;
            break;
        case 39:
            // alert('right');
            vpx += 2;
            break;
        case 40:
            // alert('down');
            vpy += 2;
            vpx += 1;
            break;
    }
};

function httpGetAsync(theUrl, callback, params)
{
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200)
            callback(xmlHttp.responseText, params);
    }
    xmlHttp.open("GET", theUrl, true); // true for asynchronous 
    xmlHttp.send(null);
}

function affine(origpoint, zoomlevel)
{

    var x = (origpoint[0] - 0.5 * origpoint[1]) * zoomlevel;
    var y = (0.8660254 * origpoint[1]) * zoomlevel;
    return [x, y];

}

function refreshCanvas(jsonString, zoomlevel)
{

    var cd = {'e': '#ff0000', 'f': '#00ff00', 'a': '#dcdc00', 'b': '#909090', 'c': '#00ffff', 'd': '#0000ff',
              'g': '#7f0000', 'h': '#007f00', 'i': '#606000', 'j': '#606060', 'k': '#007f7f', 'l': '#00007f',
              'y': '#000000'};

    var canvas = document.getElementById("arena");
    var ctx = canvas.getContext("2d");
    var inobj = JSON.parse(jsonString);
    var newEpoch = inobj.epoch;
    if (newEpoch > currentEpoch) {
        currentEpoch = newEpoch;
        var fieldNameElement = document.getElementById('epoch_count');
        fieldNameElement.innerHTML = newEpoch.toString();
        ctx.clearRect(0, 0, canvas.width, canvas.height);
        var atoms = inobj.atoms;
        var arrayLength = atoms.length;
        for (var i = 0; i < arrayLength; i++) {
            var coordpair = affine(atoms[i].coords, zoomlevel);
            ctx.fillStyle = cd[atoms[i].type];
            ctx.beginPath();
            ctx.arc(coordpair[0], coordpair[1], zoomlevel * 0.4, 0, 2 * Math.PI, false);
            ctx.fill();
        }
        var bonds = inobj.bonds;
        var arrayLength = bonds.length;
        ctx.strokeStyle = '#000000';
        for (var i = 0; i < arrayLength; i++) {
            var coordpair = affine(bonds[i][0], zoomlevel);
            ctx.beginPath();
            ctx.moveTo(coordpair[0], coordpair[1]);
            var coordpair = affine(bonds[i][1], zoomlevel);
            ctx.lineTo(coordpair[0], coordpair[1]);
            ctx.stroke();
        }
    }

    readyToReceive = true;
}

function draw()
{

    var fieldNameElement = document.getElementById('coordinates');
    fieldNameElement.innerHTML = vpx.toString() + " + " + vpy.toString() + "&omega;";

    if (readyToReceive) {
        readyToReceive = false;
        httpGetAsync("/subrect/" + currentEpoch.toString() + "/" + vpx.toString() + "/" + vpy.toString() + "/240/100", refreshCanvas, 8);
        skips = 0;
    } else {
        skips += 1;
        if (skips >= 20) { readyToReceive = true; }
    }

}

function initialise()
{
    setInterval(draw, 100);
}
