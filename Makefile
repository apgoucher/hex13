NVCCFLAGS=-std=c++11 "-Xptxas=-v" -lineinfo
CXXFLAGS=-std=c++1z -Wall -Wextra -O3 -march=native
OBJECTS=src/main.o src/hex13.o
LDFLAGS=-lmicrohttpd
CXX=g++

all: hex13.exe

hex13.exe: $(OBJECTS)
	nvcc $(LDFLAGS) $(OBJECTS) -o hex13.exe

src/hex13.o: src/kernel/monolith.h
	nvcc $(NVCCFLAGS) -c src/hex13.cu -o src/hex13.o

src/kernel/monolith.h:
	cd src/kernel; ctangle monolith.w - monolith.h

clean:
	rm -f *.o src/*.o hex13.exe scripts/*.pyc scripts/*.pyo
